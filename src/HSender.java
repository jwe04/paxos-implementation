import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

//OVERVIEW: a class that is responsible of sending heartbeats to servers
public class HSender extends Thread {
	

	ArrayList<String> addresses; //for servers
	int timeInterval; 
	int excluded; //exclude my own id
	int port;
	DatagramSocket ds;
	
	public HSender(ArrayList<String> nodes, int time, int id, int portNumber) throws SocketException {
		addresses = new ArrayList<String>();
		addresses.addAll(nodes);
		excluded = id;
		timeInterval = time;
		port = portNumber;
		
		ds = new DatagramSocket();
	}

	public void run() {	
		while(true) {
			try {
				Thread.sleep(timeInterval);
			} catch (InterruptedException e1) {
				System.out.println("Heartbeat sender was interrupted during sleep");
			}
			
			for(int i = 0; i<addresses.size();++i) {
				try {
					sendHeartBeat(i);
				} catch (IOException e) {
					System.out.println("Could not send heartbeat to process " + i);
				}
			}
			
		}
		
	}
	
	private void sendHeartBeat(int i) throws IOException {
		if(i==excluded) return;
		byte[] b = new byte[16];
		b[0] = 0;
		InetAddress add = InetAddress.getByName(addresses.get(i));
		DatagramPacket dp = new DatagramPacket(b,b.length,add,port);
		ds.send(dp);
	}
}
