//bla
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;

public class Server {
	
	//TODO --> in Globals class
	static int TIMEOUT = 1001;
	static int PORT = 8889;
	static ArrayList<String> serverAddresses;
	static int numServers = 7;
	
	//TODO --> add class constants for these
	//these are technically enums, but they will be sent as ints
	static int FORWARD = 0;
	static int PREPARE = 1;
	static int PROMISE = 2;
	static int ACCEPT = 3;
	static int LEARN = 4;

	//TODO
	//the server ID should be read as command line arguments
	static int ID = 1;
	
	//maybe start with server 0 as leader by default?
	static int leaderID = 0;
	
	static HashMap<Integer,Long> received = new HashMap<Integer,Long>(); //clientId -> last request i received from him
	static HashMap<Integer,Long> handled = new HashMap<Integer,Long>(); //clientId -> last value i handled 
	
	static DatagramSocket socket;
	
	static boolean leader;

	static long round = 0;
	
	//TODO change
	static int MAXBYTES = 16;
	static long roundNumber = 0;
	
	public static void main(String[] args) throws SocketException {
		// TODO Auto-generated method stub
		
		leader = (leaderID == ID);
		
		//fill these with addresses of servers
		fillAddresses();
		
		socket = new DatagramSocket(PORT);
		
		while(true) {
			
			byte[] bytes = new byte[MAXBYTES];
			DatagramPacket dPacket = new DatagramPacket(bytes, MAXBYTES);

			try {
				//blocking receive
				socket.receive(dPacket);
			} catch (IOException e) {
				System.out.println("Server threw an exception while waiting to receive request from client");
				continue;
			}
		
	        Thread t =  new Thread(new Handler(dPacket));
	        t.start();
		
		}

	}
	
	static class Handler implements Runnable {
		
		final DatagramPacket pack;
		
		public Handler(DatagramPacket dp) {
			pack = dp;
		}

		@Override
		public void run() {
	
			ByteArrayInputStream byteStream = new ByteArrayInputStream(pack.getData());
			DataInputStream reader = new DataInputStream(byteStream);	
			
			int senderType;
			try {
				senderType = reader.readInt(); //0 for client -- 1 for server TODO define them in constants class
			} catch (IOException e) {
				System.out.println("There was an error reading the message type");
				return;
			}
			
			if(senderType == 0) {//client
				//a client sent this
				int clientId;
				long requestNum;
				
				try {
					clientId = reader.readInt();
					requestNum = reader.readLong();
				} catch (IOException e) {
					System.out.println("There was an error reading the sender ID and the request number");
					//return will abort this thread
					return;
				}
				
				boolean check = true;
				//there are 3 possibilities: 
				//1) Already handled the request
				//2) Received but haven't handled the request
				//3) A new request
				
				//must use synchronized to avoid race conditions between threads
				synchronized(handled) {
					synchronized(received) {
						if (handled.containsKey(clientId) && handled.get(clientId) == requestNum) {
							//if the request is in the handled requests set
							//set check to false (no need to check the other possibilities)
							check = false;
							if (leader) {
								//If I am the leader send acknowledgment to sending process
								try {
									sendAck(pack.getAddress(), pack.getPort());
									sendDone(pack.getAddress(), pack.getPort());
								} catch (IOException e) {
									System.out.println("Server " + ID
											+ " threw an exception while replying to already handled req");
									return;
								}
							} 
							else {
								//else forward this request to to leader (it is the only one that can propose)
								try {
									forwardReq(clientId, requestNum);
								} catch (IOException e) {
									System.out.println("Server " + ID
											+ " threw an exception while forwarding already handled req");
									return;
								}
							}
							
						} 
						if (check) {
							if(received.containsKey(clientId) && received.get(clientId) == requestNum ) {
								//if the request is in the received requests set
								//set check to false (no need to check the other branches)
								check = false;
								if (leader) {
									try {
										sendAck(pack.getAddress(), pack.getPort());
									} catch (IOException e) {
										System.out.println("Server " + ID
												+ " threw an exception while replying to already received req");
										return;
									}
								} else {
									// forward to leader
									try {
										forwardReq(clientId, requestNum);
									} catch (IOException e) {
										System.out.println("Server " + ID
												+ " threw an exception while forwarding already handled req");
										return;
									}
								}

							}
						}
						
						if(check) {
							//means it is a new request
							received.put(clientId, requestNum);
							if(leader) {
								try {
									sendAck(pack.getAddress(), pack.getPort());
								} catch (IOException e) {
									System.out.println("Server " + ID
											+ " threw an exception while replying to new req");
									return;
								}
								
								//Now I must initiate Paxos
								//by sending a prepare message
								
								//make proposal value a tuple
								try {
									sendProposal(clientId,requestNum,numServers*round + ID);
									roundNumber++; //TODO or round
								} 
								catch (IOException e) {
									System.out.println("Server " + ID
											+ " threw an exception while sending new proposal");
									return;
								}
								
								//now what? :D
								//CODE
								
							}
							else {
								try {
									forwardReq(clientId, requestNum);
								}
								catch (IOException e) {
									System.out.println("Server " + ID
											+ " threw an exception while forwarding new req");
									return;
								}						
							}
						}
					}
				}
			}
			else {
				//another server sent this
				int type;				
				try {
					type = reader.readInt();//TODO --> CONSTANTS CLASS
				} catch (IOException e) {
					System.out.println("There was an error reading the sender request type");
					return;
				}	
			
				if(type==FORWARD) {
					//means I am the leader	
					int sender;
					long requestNum;
					try {
						sender = reader.readInt();
						requestNum = reader.readLong();
					} 
					catch (IOException e) {
						System.out.println("There was an error reading the sender ID and the request number");
						return;
					}
					
					
					//TODO redundant code --> separate method
					synchronized(handled) {
						synchronized(received) {
							boolean check = true;
							
							if(handled.containsKey(sender)) {
								if(handled.get(sender) == requestNum) {
									check = false;
									
									//send ack to the client, not the sending process
									
									//send done to the client, not the sending process
								}
							}
							if(check) {
								if(received.containsKey(sender)) {
									if(received.get(sender) == requestNum) {
										check = false;
										
										//send ack to the client, not the sending process
										
									}
								}
							}
							if(check) {
								//means this is a new value
								received.put(sender, requestNum);
								
								//send ack to the client, not the sending process
								
								//start proposal
								try {
									sendProposal(sender,requestNum,numServers*round + ID);
									roundNumber++;
								} 
								catch (IOException e) {
									System.out.println("Server " + ID
											+ " threw an exception while sending new proposal");
									return;
								}
								
								//now what? :D
								//must store this req and round number
								//CODE
								
							}
							
						}
					}
				}
				
				//TODO
				else if(type == PREPARE) {
					//must check if I have already promised to a higher round number
					
					//if not, send promise
				}
				
				//TODO
				else if(type == PROMISE) {
					//I must have sent a prepare and this is the corresponding promise
					
					//every time I get a promise, I must increment a counter on some request
					
					//if the counter reaches 2f + 1, send accept
				}
				//TODO
				else if(type == ACCEPT) {
					//leader is sending an accept
					
					//must check if I have already promised to a higher round number
					
					//if not, send learn
				}
				//TODO
				else {
					//this message is a learn
					
					//CODE
				}
			}
		
		}
		
	}
	
	static class Proposal {
		int client;
		long req;
		
		public Proposal(int c, long r) {
			client = c;
			req = r;
		}	
	}
	
	public static void sendProposal(int client, long req, long roundNumber) throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream writer = new DataOutputStream(byteStream);
				
		writer.writeInt(1);
		writer.writeInt(PREPARE);
		writer.writeLong(roundNumber);
		writer.writeInt(client);
		writer.writeLong(req);
		writer.flush();
		writer.close();
		
		DatagramPacket dPacket = new DatagramPacket(byteStream.toByteArray(), byteStream.toByteArray().length,
				PORT);
		
		for(int i = 0; i < serverAddresses.size(); ++i) {
			dPacket.setAddress(InetAddress.getByName(serverAddresses.get(i)));
			socket.send(dPacket);
		}
		
	}
	
	public static void forwardReq(int sender, long req) throws IOException {
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream writer = new DataOutputStream(byteStream);
	
		// Sender Forward Message format:
		// Start with 1 (to identify itself as a server message)
		// 0 to indicate this is simply a forwarded request
		// ID of client
		// request number
		writer.writeInt(1);
		writer.writeInt(FORWARD);
		writer.writeInt(sender);
		writer.writeLong(req);
		writer.flush();
		writer.close();

		DatagramPacket dPacket;
		dPacket = new DatagramPacket(byteStream.toByteArray(), byteStream.toByteArray().length,
				InetAddress.getByName(serverAddresses.get(leaderID)), PORT);
		socket.send(dPacket);

	}
	
	//TODO
	public static void sendDone(InetAddress a, int port) throws IOException {
		sendAck(a,port);
	}

	public static void sendAck(InetAddress a, int port) throws IOException {

		byte[] b = new byte[4];
		DatagramPacket pack = new DatagramPacket(b, b.length, a, port);

		socket.send(pack);
	}
	
	public static void fillAddresses() {
		serverAddresses = Globals.getServerAddresses();
		numServers = serverAddresses.size();
	}
	
}
