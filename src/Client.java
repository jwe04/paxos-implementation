
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Client {

	static int TIMEOUT = 1000;
	static int HANDLETIMEOUT = 1000;

	static int PORT = 8889;
	static int SENDPORT = 8091;
	static ArrayList<String> serverAddresses = new ArrayList<String>();
	static int ID = 1;

	public static void main(String[] args) throws UnknownHostException, SocketException {
		
		
		fillAddresses();
		long requestNumber = 0;
		
		DatagramSocket s = new DatagramSocket(PORT);
		
		while(true) {

			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			DataOutputStream writer = new DataOutputStream(byteStream);
		
			try {
				//Client Message format:
				//Start with 0 (to identify itself as a client message)
				//ID of client
				//request number
				//total 16 bytes
				
				writer.writeInt(0);
				writer.writeInt(ID);
				writer.writeLong(requestNumber);
				writer.flush();
				writer.close();
			} catch (IOException e) {
				System.out.println("Client "+ ID + " threw an exception when writing to the byteStream");
				continue;
			}
	
			DatagramPacket dPacket = new DatagramPacket(byteStream.toByteArray(), byteStream.toByteArray().length,
					SENDPORT);
				
			//send phase
			//while the server hasn't acknowledged the request, try sending the message
			while(true) {
				try {
					//TODO
					//Here we are sending to all servers --> check it
					for(int i = 0; i < serverAddresses.size(); ++i ) {
						InetAddress add = InetAddress.getByName(serverAddresses.get(i));
						dPacket.setAddress(add);
						s.send(dPacket);
					}

					
					/// wait for acknowledgment (using timeout).
					// If server replies within set time, break out of while loop
					// else repeat
					
					// server must implement behavior such that if it receives a request
					// it has already handled, it should resend the acknowledgment
					
					s.setSoTimeout(TIMEOUT);
					s.receive(dPacket);
					
					//if I don't throw an exception, it means I received acknowledgment and I must break
					break;
				}
				catch(IOException e) {
					System.out.println("Process " + ID + " threw an exception while sending the DatagramPacket OR while"
							+ "receiving acknowledgment from server");		
				}
			}
			
			// wait for server to handle request.
			// Server must send me a message only when he has handled my request
			
			try {
				s.setSoTimeout(HANDLETIMEOUT);
				s.receive(dPacket);
			} catch (IOException e) {
				System.out.println("Process " + ID + " threw an exception while waiting to receive the handling message");
				continue;//in case the server failed resend request (i.e. go back to while true)
			}
			
			
			//here I do processing
			//here server handled request
			try {
				//sleep for 10 seconds
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			requestNumber++;
		}

	}
	
	//TODO
	public static void fillAddresses() {
		
	}

}
